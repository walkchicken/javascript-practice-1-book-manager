
import Controller from "./controllers/books";
import Model from "./models/book";
import View from "./views/books";

const model = new Model(), view = new View(model);
const controller = new Controller(model, view);

