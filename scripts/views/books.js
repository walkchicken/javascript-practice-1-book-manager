export default class View {

  constructor() {
    this.addForm = document.getElementById("add");
    this.updateForm = document.getElementById("add");
    this.detailsForm = document.getElementsByClassName("details__form");
    this.searchForm = document.getElementById("search-form");
    this.searchForm.addEventListener("click", (e) => {
      const title = document.getElementById("search").value;
      e.preventDefault();
      this.handleSearch(title);
    });

    //select sort and add event change option sort value
    this.sortForm = document.getElementById("sort");
    this.sortForm.addEventListener("change", (e) => {
      const value = this.sortForm.options[this.sortForm.selectedIndex].value;
      if (value == "a-z") {
        //call to function sort
        this.handleSortAZ(value);
      } else {
        this.handleSortZA(value);
      }
    });

    //add book
    this.validatorAddBook = document.getElementById("book-form");
    this.validatorAddBook.addEventListener("submit", (e) => {
      const errorMessage = document.getElementById("error-message");
      const bookPhotoElement = document.getElementById("photo");
      const bookTitleElement = document.getElementById("title");
      const bookIntroElement = document.getElementById("intro");
      const bookMadeElement = document.getElementById("made");
      const bookPhoto = document.getElementById("photo").value;
      const bookTitle = document.getElementById("title").value;
      const bookIntro = document.getElementById("intro").value;
      const bookMade = document.getElementById("made").value;

      // set style to error message
      errorMessage.style.padding = "10px";
      errorMessage.style.cssText = "color:red;font-size:28px;";

      var text;

      if (bookMade.length < 5 || bookMade === "") {
        text = "Please provide your made!";
        bookMadeElement.style.cssText = "border:1px solid red";
        errorMessage.innerHTML = text;
      } else {
        bookMadeElement.style.cssText = "border:1px solid green";
      }

      if (bookIntro.length < 5 || bookIntro === "") {
        text = "Please provide your intro!";
        bookIntroElement.style.cssText = "border:1px solid red";
        errorMessage.innerHTML = text;
      } else {
        bookIntroElement.style.cssText = "border:1px solid green";
      }

      if (bookTitle.length < 5 || bookTitle === "") {
        text = "Please provide your title!";
        bookTitleElement.style.cssText = "border:1px solid red";
        errorMessage.innerHTML = text;
      } else {
        bookTitleElement.style.cssText = "border:1px solid green";
      }

      if (bookPhoto.length < 5) {
        text = "Please provide your photo!";
        bookPhotoElement.style.cssText = "border:1px solid red";
        errorMessage.innerHTML = text;
      } else {
        bookPhotoElement.style.cssText = "border:1px solid green";
      }

      const is_validated = bookTitle && bookPhoto && bookIntro && bookMade;

      if (is_validated) {
        this.handleAddBook({
          id: new Date(),
          bookPhoto,
          bookTitle,
          bookIntro,
          bookMade,
          bookDone: 0,
          finished: false,
        });
      }
    });

    const closeAdd = document.querySelector("#close-button");
    closeAdd.addEventListener("click", () => {
      this.addForm.style.display = "none";
    });

    const cancelAdd = document.querySelector("#cancel-add");
    cancelAdd.addEventListener("click", () => {
      this.addForm.style.display = "none";
    });
  }

  //render books
  renderBooks(books = [],) {
    const tableBodyNode = document.getElementById("book-list");
    tableBodyNode.innerHTML = ""
    books.map((book) => {
      const buttonDelete = document.createElement("button");
      buttonDelete.setAttribute("buttonId", book.id);
      buttonDelete.className = "btn btn-icons btn-delete delete-book control-book-btn";

      buttonDelete.addEventListener("click", (e) => {
        var proceed = confirm("Are you sure you want to delete?");
        if (proceed) {
          this.handleDelete(book.id);
        } else {
          e.preventDefault();
        }
      });

      const buttonUpdate = document.createElement("button");
      buttonUpdate.setAttribute("buttonId", book.id);
      buttonUpdate.className = "btn btn-icons btn-update update-book control-book-btn";

      buttonUpdate.addEventListener("click", () => {
        this.open_update(book);
      });

      const buttonDetails = document.createElement("button");
      buttonDetails.setAttribute("buttonId", book.id);
      buttonDetails.className = "btn btn-icons btn-details details-book control-book-btn";

      buttonDetails.addEventListener("click", () => {
        this.handleDetails(book);
      });

      const itemBook = document.createElement("div");
      itemBook.className = "card-item"

      const itemIntroBook = document.createElement("div");
      itemIntroBook.className = "card-item__intro"

      const pictureBook = document.createElement("picture");
      const photoBook = document.createElement("img");
      photoBook.id = "photo";
      photoBook.setAttribute("src", book.bookPhoto);
      photoBook.textContent = book.bookPhoto;
      pictureBook.appendChild(photoBook);

      const bookTitle = document.createElement("h2");
      bookTitle.id = "title";
      bookTitle.className = "card-item__title"
      bookTitle.textContent = book.bookTitle;

      itemIntroBook.appendChild(pictureBook);
      itemIntroBook.appendChild(bookTitle);

      const itemControlBook = document.createElement("div");
      itemControlBook.className = "card-item__control control__icons"
      itemControlBook.appendChild(buttonDelete);
      itemControlBook.appendChild(buttonUpdate);
      itemControlBook.appendChild(buttonDetails);

      itemBook.appendChild(itemIntroBook);
      itemBook.appendChild(itemControlBook);
      tableBodyNode.appendChild(itemBook);

    });
  }

  //render details book
  renderDetailsBook(book) {
    const cardDetails = document.getElementById("details__form");
    cardDetails.className = "book__details";
    cardDetails.style.display = "block";
    cardDetails.innerHTML = ""

    const detailsPhotoBook = document.createElement("picture");
    detailsPhotoBook.className = "photo__details";
    const detailsPhoto = document.createElement("img");
    detailsPhoto.id = "photo";
    detailsPhoto.setAttribute("src", book.bookPhoto);
    detailsPhoto.textContent = book.bookPhoto;
    detailsPhotoBook.appendChild(detailsPhoto);

    const detailsIntro = document.createElement("div");
    detailsIntro.className = "book__details-intro";
    const detailsTitleBook = document.createElement("h2");
    detailsTitleBook.id = "title";
    detailsTitleBook.textContent = book.bookTitle;
    const detailsIntroBook = document.createElement("p");
    detailsIntroBook.id = "intro";
    detailsIntroBook.textContent = book.bookIntro;
    const detailsMadeBook = document.createElement("h4");
    detailsMadeBook.id = "author";
    detailsMadeBook.textContent = book.bookMade;

    const buttonExit = document.createElement("button");
    buttonExit.className = "btn btn-cancel close-details";
    buttonExit.textContent = "EXIT";

    buttonExit.addEventListener("click", () => {
      cardDetails.style.display = "none";
      cardDetails.innerHTML = "";
    });

    const detailsControl = document.createElement("div");
    detailsControl.className = "book__details-control";
    detailsControl.appendChild(buttonExit);

    detailsIntro.appendChild(detailsTitleBook);
    detailsIntro.appendChild(detailsIntroBook);
    detailsIntro.appendChild(detailsMadeBook);
    detailsIntro.appendChild(detailsControl);

    const contentDetails = document.createElement("div");
    contentDetails.className = "book__details-content";
    contentDetails.appendChild(detailsPhotoBook);
    contentDetails.appendChild(detailsIntro);

    cardDetails.appendChild(contentDetails);
  }

  //create function update book
  open_update = (book) => {
    const updateForm = document.getElementById("update__form");
    updateForm.className = "update__form";
    updateForm.innerHTML = "";
    console.log(book);

    const updateFormBook = document.createElement("form");
    updateFormBook.className = "add__form-book";

    const errorMess = document.createElement("span");
    errorMess.id = "error-mess";
    errorMess.style.padding = "10px";
    errorMess.style.cssText = "color:red;font-size:28px;";

    const labelBookPhoto = document.createElement("label");
    labelBookPhoto.textContent = "Book Photo";

    const inputBookPhoto = document.createElement("input");
    inputBookPhoto.type = "text";
    inputBookPhoto.id = "update-photo";
    inputBookPhoto.className = "form-control";
    inputBookPhoto.value = book.bookPhoto;

    const labelBookTitle = document.createElement("label");
    labelBookTitle.textContent = "Title & Author";

    const inputBookTitle = document.createElement("input");
    inputBookTitle.type = "text";
    inputBookTitle.id = "update-title";
    inputBookTitle.className = "form-control";
    inputBookTitle.value = book.bookTitle;

    const labelBookIntro = document.createElement("label");
    labelBookIntro.textContent = "Book Intro";

    const inputBookIntro = document.createElement("input");
    inputBookIntro.type = "text";
    inputBookIntro.id = "update-intro";
    inputBookIntro.className = "form-control";
    inputBookIntro.value = book.bookIntro;

    const labelBookMade = document.createElement("label");
    labelBookMade.textContent = "Made By";

    const inputBookMade = document.createElement("input");
    inputBookMade.type = "text";
    inputBookMade.id = "update-made";
    inputBookMade.className = "form-control";
    inputBookMade.value = book.bookMade;

    //create button update and add event click to button update
    const buttonUpdate = document.createElement("button");
    buttonUpdate.className = "btn btn-submit-form btn-true";
    buttonUpdate.textContent = "UPDATE";
    buttonUpdate.addEventListener("click", (e) => {

      const bookPhoto = document.getElementById("update-photo").value;
      const bookTitle = document.getElementById("update-title").value;
      const bookIntro = document.getElementById("update-intro").value;
      const bookMade = document.getElementById("update-made").value;
      const errorMessage = document.getElementById("error-mess");
      const bookPhotoValidated = document.getElementById("update-photo");
      const bookTitleValidated = document.getElementById("update-title");
      const bookIntroValidated = document.getElementById("update-intro");
      const bookMadeValidated = document.getElementById("update-made");
      var text;

      if (bookPhoto.length < 5) {
        text = "Please provide your photo!";
        bookPhotoValidated.style.cssText = "border:1px solid red";
        errorMessage.innerHTML = text;
        e.preventDefault();
      }

      if (bookTitle.length < 5 || bookTitle === "") {
        text = "Please provide your title!";
        bookTitleValidated.style.cssText = "border:1px solid red";
        errorMessage.innerHTML = text;
        e.preventDefault();
      }

      if (bookIntro.length < 5 || bookIntro === "") {
        text = "Please provide your intro!";
        bookIntroValidated.style.cssText = "border:1px solid red";
        errorMessage.innerHTML = text;
        e.preventDefault();
      }

      if (bookMade.length < 5 || bookMade === "") {
        text = "Please provide your made!";
        bookMadeValidated.style.cssText = "border:1px solid red";
        errorMessage.innerHTML = text;
        e.preventDefault();
      }

      if (bookTitle && bookPhoto && bookIntro && bookMade) {
        bookPhotoValidated.style.cssText = "border:1px solid green";
        bookTitleValidated.style.cssText = "border:1px solid green";
        bookIntroValidated.style.cssText = "border:1px solid green";
        bookMadeValidated.style.cssText = "border:1px solid green";
          this.handleUpdate({
            id: book.id,
            bookPhoto,
            bookTitle,
            bookIntro,
            bookMade,
            bookDone: 0
          })
      }
    });

    const buttonCancel = document.createElement("button");
    buttonCancel.className = "btn btn-submit-form btn-cancel cancel-add";
    buttonCancel.textContent = "CANCEL";

    buttonCancel.addEventListener("click", () => {
      cardDetails.style.display = "none";
      cardDetails.innerHTML = "";
    });

    updateFormBook.appendChild(errorMess);
    updateFormBook.appendChild(labelBookPhoto);
    updateFormBook.appendChild(inputBookPhoto);
    updateFormBook.appendChild(labelBookTitle);
    updateFormBook.appendChild(inputBookTitle);
    updateFormBook.appendChild(labelBookIntro);
    updateFormBook.appendChild(inputBookIntro);
    updateFormBook.appendChild(labelBookMade);
    updateFormBook.appendChild(inputBookMade);
    updateFormBook.appendChild(buttonUpdate);
    updateFormBook.appendChild(buttonCancel);
    updateForm.appendChild(updateFormBook);

  };

  //set style to open add form
  open = () => {
    this.addForm.style.display = "block";
    console.log("open");
  };

  //set style to open detail add form
  open_details = () => {
    this.detailsForm.style.display = "block";
  };

  //set style to close detail add form
  close_details = () => {
    this.detailsForm.style.display = "none";
  };

  close_update = () => {
    this.updateForm.style.display = "none";
  };

  //connect open add form to controller
  bindOpenForm(openForm) {
    const button = document.getElementById("btn-add");
    button.addEventListener("click", openForm);
  }

  //connect open detail to controller
  bindOpenDetailsForm(openForm) {
    const button = document.getElementsByClassName("btn-details");
    button.addEventListener("click", openForm);
  }

  handleAddBook = (book) => {
    this.handleAddBook(book);
  }

  //connect event add book to controller
  bindAddBook(fn) {
    this.handleAddBook = fn;
  }

  handleSearch = (title) => {
    this.handleSearchBook(title);
  }

  //connect search book to controller
  bindSearchBook = (fn) => {
    this.handleSearchBook = fn;
  }

  //connect delete book to controller
  bindDeleteBook = (fn) => {
    this.handleDeleteBook = fn;
  }

  handleDelete = (id) => {
    this.handleDeleteBook(id);
  }

  //connect update book to controller
  bindUpdateBook = (fn) => {
    this.handleUpdateBook = fn;
  }

  handleUpdate = (newBook) => {
    this.handleUpdateBook(newBook);
  }

  //connect detail book to controller
  bindDetailsBook = (fn) => {
    this.handleDetailsBook = fn;
  }

  handleDetails = (book) => {
    this.handleDetailsBook(book);
  }

  //connect sort to controller
  bindSortAZBook = (fn) => {
    this.handleSortAZBook = fn;
  }

  handleSortAZ = (book) => {
    this.handleSortAZBook(book);
  }

  //connect to controller
  bindSortZABook = (fn) => {
    this.handleSortZABook = fn;
  }

  handleSortZA = (book) => {
    this.handleSortZABook(book);
  }

}


