export default class Model {

  constructor() {
    this.books = JSON.parse(localStorage.getItem("books")) || [];
    this.searchResult = JSON.parse(localStorage.getItem("books")) || [];
  }

  //add book
  addBook = (book) => {
    this.books.push(book);
    localStorage.setItem("books", JSON.stringify(this.books));
  };

  //delete book
  deleteBook = (id) => {
    this.books = this.books.filter(book => book.id != id);
    localStorage.setItem("books", JSON.stringify(this.books));
  }

  //update book
  updateBook = (edit) => {
    const index = this.books.findIndex(book => book.id === edit.id);
    if (index >= 0) {
      this.books[index] = edit;
      localStorage.setItem("books", JSON.stringify(this.books));
    }
  }

  //details book
  detailsBook = (id) => {
    const details = this.books.find(book => book.id === id);
    console.log(details);
  }

  //search book
  searchBook = (title) => {
    this.searchResult = this.books.filter((book) =>
      book.bookTitle.toLowerCase().includes(title.toLowerCase())
    );
  };

  // sort book a-z
  sortAZBook = () => {
    this.books.sort((titleAlpha, titleBeta) => titleAlpha.bookTitle.localeCompare(titleBeta.bookTitle));
    localStorage.setItem("books", JSON.stringify(this.books));
  }

  //sort book z-a
  sortZABook = () => {
    this.books.sort((titleAlpha, titleBeta) => titleBeta.bookTitle.localeCompare(titleAlpha.bookTitle));
    localStorage.setItem("books", JSON.stringify(this.books));
  }
}
