export default class Controller {

  constructor(model, view) {
    this.model = model;
    this.view = view;
    this.view.bindDeleteBook(this.deleteBook);
    this.view.bindDetailsBook(this.detailsBook);
    this.view.renderBooks(this.model.books);
    this.view.bindAddBook(this.handleAddBook);
    this.view.bindUpdateBook(this.handleUpdateBook);
    this.view.bindOpenForm(this.handleOpenForm);
    this.view.bindOpenDetailsForm(this.handleOpenDetailsForm);
    this.view.bindSearchBook(this.handleSearchBook);
    this.view.bindSortAZBook(this.handleSortAZBook);
    this.view.bindSortZABook(this.handleSortZABook);
  }

  onBookChange = (books) => {
    this.view.renderBooks(books);
  };

  handleAddBook = (book) => {
    this.model.addBook(book);
    this.onBookChange(this.model.books);
  };

  handleOpenForm = () => {
    this.view.open();
  };

  handleOpenDetailsForm = () => {
    this.view.open_details();
  };

  handleCloseDetailsForm = () => {
    this.view.close_details();
  }

  deleteBook = (id) => {
    this.model.deleteBook(id);
    this.onBookChange(this.model.books);
  }

  detailsBook = (book) => {
    this.view.renderDetailsBook(book);
  }

  //call update from model
  handleUpdateBook = (book) => {
    this.model.updateBook(book);
  };

  //call search from model
  handleSearchBook = (query) => {
    this.model.searchBook(query);
    this.onBookChange(this.model.searchResult);
  };

  handleSortAZBook = (book) => {
    this.model.sortAZBook(book);
    this.onBookChange(this.model.books);
  }

  //call sort from model
  handleSortZABook = (book) => {
    this.model.sortZABook(book);
    this.onBookChange(this.model.books);
  }
}
