# Javascript Practice 1 Book Manager

# javascript-practice-1-book-manager

## DEVELOPMENT TEAM

- Lead - Thong Nguyen.
- Tech guy - Nhat Duong.

## PLAN

- Plan on gitlab [Board](https://gitlab.com/walkchicken/javascript-practice-1-book-manager/-/boards/3948106).
- Plan on doc [Internship JavaScript Practice 1 Book Manager](https://docs.google.com/document/d/1Il0liklczRm3nezYnY6-CV2eTi1CHeshopk8IqBqrTw/edit#).

## PROJECT TOOLS

- Parcel.
- VScode.

## TECHNICAL STACK

- HTML5.
- CSS3.
- Sass.
- JavaScript

## ENVIRONMENT

- Develop environment [javascript-practice-1-book-manager](https://nhatduong-books-manager.herokuapp.com/).

## REFERENCES

- [JavaScript Beginner's Handbook](https://drive.google.com/file/d/13ul4nq_hbW5RBymdeTfSRCgwVoiSwvUb/view).
- [JavaScript Basic](https://www.youtube.com/watch?v=0SJE9dYdpps&list=PL_-VfJajZj0VgpFpEVFzS5Z-lkXtBe-x5).
- [DOM manipulation](https://www.digitalocean.com/community/tutorial_series/understanding-the-dom-document-object-model).
- [Code style](https://github.com/airbnb/javascript#types).
- [Classes in JS](https://www.digitalocean.com/community/tutorials/understanding-classes-in-javascript).
- [JS exercises](https://www.w3resource.com/javascript-exercises/javascript-array-exercises.php?fbclid=IwAR0zx-ddY_GJl8YQHiJpInoFqxL3U3fI_Pmoq_HmrDqOaaL_XUxLYvvRozo).
- [Learn JS - full course for beginners](https://www.youtube.com/watch?v=PkZNo7MFNFg).

## ISSUES

- Issues and how to solve them

## SETUP & RUN

```
git clone https://gitlab.com/walkchicken/javascript-practice-1-book-manager.git
cd javascript-practice-1-book-manager
yarn install
yarn build
yarn start
output: http://localhost:1234
```
